<br />

<div class='limiter clearfix'>
  <ul class='breadcrumb'>
    <?php print $breadcrumb ?>
  </ul>
</div>

<div id='page-title'><div class='limiter clearfix'>
  <div class='pull-right'>
    <?php if ($primary_local_tasks): ?>
      <ul class='nav nav-pills clearfix'><?php print render($primary_local_tasks) ?></ul>
    <?php endif; ?>
  </div>
  <?php print render($title_prefix); ?>
  <h1 class='page-title <?php print $page_icon_class ?>'>
    <?php if (!empty($page_icon_class)): ?><span class='icon'></span><?php endif; ?>
    <?php if ($title) print $title ?>
  </h1>
  <?php if ($action_links): ?>
    <?php if (isset($action_links[0])): ?>
    <?php foreach ($action_links as $action): ?>
      &nbsp;&nbsp;&nbsp;<a href="<?php echo url($action['#link']['href'], array("ABSOLUTE" => true)) ?>" title="<?php echo $action['#link']['title'] ?>" class="btn btn-small"><?php echo $action['#link']['title'] ?></a>
    <?php endforeach ?>
    <?php else: ?>
      &nbsp;&nbsp;&nbsp;<a href="<?php echo url($action_links['#link']['href'], array("ABSOLUTE" => true)) ?>" title="<?php echo $action_links['#link']['title'] ?>" class="btn btn-small"><?php echo $action_links['#link']['title'] ?></a>
    <?php endif ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
</div></div>

<?php if ($show_messages && $messages): ?>
<div id='console'><div class='limiter clearfix'><?php print $messages; ?></div></div>
<?php endif; ?>

<div id='page'><div id='main-content' class='limiter clearfix'>
  <?php if ($page['help']) print render($page['help']) ?>
  <div id='content' class='page-content clearfix'>
    <?php if (!empty($page['content'])) print render($page['content']) ?>
  </div>
</div></div>

<div id='footer' class='clearfix'>
  <?php if ($feed_icons): ?>
    <div class='feed-icons clearfix'>
      <label><?php print t('Feeds') ?></label>
      <?php print $feed_icons ?>
    </div>
  <?php endif; ?>
</div>
